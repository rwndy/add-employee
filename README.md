## About the project

Build Dashboard Admin

### Built with Angular Framework


## Getting Started

A. Develop apps:
  1. Clone the repo
     ```sh
     git clone https://gitlab.com/rwndy/add-employee.git
     ```
  2. Install packages
     ```sh
      npm install
     ```
  3. Start project
     ```sh
     ng serve
     ```
